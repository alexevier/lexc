AS=nasm
CC=gcc
INCLUDE=include

default: build/x86_64/lexc.a

test: build/test.o build/x86_64/lexc.a
	@echo -e "[\e[1;32mlink\e[0m]: $@"
	@ld $^ -o build/test
	@build/test

build/x86_64/lexc.a: build/x86_64/start.o build/x86_64/linux/syscall.o build/exit.o
	@echo -e "[\e[1;32mlibrary\e[0m]: $@"
	@ar rcs $@ $^

build/x86_64/start.o: src/x86_64/start.asm
	@echo -e "[\e[1;32mbuild\e[0m]: $@"
	@mkdir -p build/x86_64/
	@$(AS) -f elf64 $^ -o $@

build/x86_64/linux/syscall.o: src/x86_64/linux/syscall.asm
	@echo -e "[\e[1;32mbuild\e[0m]: $@"
	@mkdir -p build/x86_64/linux
	@$(AS) -f elf64 $^ -o $@

build/exit.o: src/exit.c
	@echo -e "[\e[1;32mbuild\e[0m]: $@"
	@mkdir -p build/x86_64/
	@$(CC) -c $^ -o $@ -I$(INCLUDE)

build/test.o: src/test.c
	@echo -e "[\e[1;32mbuild\e[0m]: $@"
	@mkdir -p build/x86_64/
	@$(CC) -c $^ -o $@ -I$(INCLUDE) -fno-stack-protector

clean:
	@rm -rf build
