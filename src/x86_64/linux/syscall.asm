section .text

%define SYS_WRITE 0x01
%define SYS_EXIT 0x3C

global write
global _exit

; write syscall
;	params:
;	- rdi: fd
;	- rsi: buf ptr
;	- rdx: count
write:
	mov rax, SYS_WRITE
	syscall
	ret

; exit syscall
;	params:
;	- rdi: exit_code
_exit:
	mov rax, SYS_EXIT
	syscall
