section .text

global _start

extern main
extern _exit

_start:
	xor rbp, rbp
	push rbp
	call main
	pop rbp
	mov rdi, rax
	call _exit
